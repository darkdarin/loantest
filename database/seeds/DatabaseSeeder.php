<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Создаем случайных пользователей
        factory(App\User::class, 50)->create();
        $user_ids = App\User::pluck('id');

        factory(App\Comment::class, 500)->make()->each(function ($comment) use ($user_ids) {
            /** @var \App\Comment $comment */
            $comment->user_id = $user_ids[mt_rand(0, count($user_ids) - 1)];
            $comment->save();
        });

    }
}
