<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Comment
 * @package App
 * @property integer $id
 * @property string $user_id
 * @property string $text
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Comment extends Model
{
    //
}
