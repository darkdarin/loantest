<?php

namespace App;

use DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 * @package App
 * @property integer $id
 * @property string $name
 * @property float $balance
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class User extends Model
{
    const ERROR_BALANCE = 100;
    const ERROR_SENDER = 110;
    const ERROR_RECEIVER = 120;
    const ERROR_TRANSFER = 130;

    /**
     *
     * @param $from
     * @param $to
     * @param $amount
     * @return int
     */
    public static function transfer($from, $to, $amount)
    {
        DB::beginTransaction();

        //проверяем, что отправитель действительно существует
        $sender = User::where('id', $from)->lockForUpdate()->first();
        if (!$sender) {
            DB::rollback();
            return self::ERROR_SENDER;
        }

        //проверяем, что баланса хватает
        if ($sender->balance < $amount) {
            DB::rollback();
            return self::ERROR_BALANCE;
        }

        //проверяем, что получатель действительно существует
        $receiver = User::where('id', $to)->lockForUpdate()->first();
        if (!$receiver) {
            DB::rollback();
            return self::ERROR_RECEIVER;
        }

        //все ок, осуществляем операцию
        $sender->balance = $sender->balance - $amount;
        $receiver->balance = $receiver->balance + $amount;

        if (!$sender->save() || !$receiver->save()) {
            DB::rollback();
            return self::ERROR_TRANSFER;
        }

        DB::commit();

        return true;
    }
}
